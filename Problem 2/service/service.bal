import ballerina/graphql;


public table<Statistic> key(region) statsTable = table[]; 

 public type Statistic record {|
    string date;
    readonly string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested;
|};
 

isolated service /graphql on new graphql:Listener(9090) {

    function init() {
        statsTable = table [
                {date: "2021-09-12", region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200}

            ];
    }

    resource function get stat() returns Statistic[] {
       return statsTable.toArray();
    }

    remote function updateStats(string region, string? date, int? deaths, int? confirmed_cases, int? recoveries, int? tested) returns Statistic|error {
        Statistic? stat = statsTable.get(region);
        if stat != (){
            if date != () {
                stat.date = date;
            }
            if deaths != () {
                stat.deaths = deaths;
            }
            if confirmed_cases != () {
                stat.confirmed_cases = confirmed_cases;
            }
            if recoveries != () {
                stat.recoveries = recoveries;
            }
            if tested != () {
                stat.tested = tested;
            }
            return stat;
        } else {

            return error("Not found"); 
        }

    }
}

