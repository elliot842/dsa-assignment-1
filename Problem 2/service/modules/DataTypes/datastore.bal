public type Statistic record {|
    string date;
    readonly string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested;
|};

public table<Statistic> key(region) statisticsTable = table[]; 