import ballerina/io;
import ballerina/grpc;
import ballerina/protobuf;

listener grpc:Listener ep = new (9090);


// //Data storage 
public User[] UserDataBase = []; public Course[] CourseDataBase = []; public Course[] CourseAssignmentsDataBase = []; public Course_Assignment[] SAssignmentsDataBase = []; public Course_Assignment[] MAssignmentsDataBase = [];

@grpc:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}

service "CMS" on ep {

    remote function assign_courses(stream<Course, grpc:Error?> clientStream) returns result|error {

        result results = {};
        check clientStream.forEach(function(Course c) {
            CourseAssignmentsDataBase.push(c);
            results = {code: "200", message: "All Courses Assigned Succesfully"};
        });
 
        return results;
    }

    remote function create_users(stream<User, grpc:Error?> clientStream) returns result|error {

        result resultValue = {code: "404", message: "Error"};

 
        check clientStream.forEach(function(User u) {
            UserDataBase.push(u);
            resultValue = {code: "200", message: "All Users Created Successfully"};
        });
 

        return resultValue;
    }

    remote function submit_assignments(stream<Course_Assignment, grpc:Error?> clientStream) returns result|error {
        result results = {};
        check clientStream.forEach(function(Course_Assignment ca) {
            SAssignmentsDataBase.push(ca);
            results = {code: "200", message: "All Assignemnts Have Been Submitted Succesfully"};
        });

 

        return results;
    }

    remote function submit_marks(stream<Course_Assignment, grpc:Error?> clientStream) returns result|error {
        result resultValue = {code: "404", message: "Error"};
        check clientStream.forEach(function(Course_Assignment u) {
            MAssignmentsDataBase.push(u);
            resultValue = {code: "200", message: "All Assignments Graded Successfully"};});
        return resultValue;
    }

//Couldnt Complete this function in time
    remote function register(stream<Course_Assignment, grpc:Error?> clientStream) returns result|error {

        result resultValue = {code: "", message: ""};

        return resultValue;
    }

    remote function request_assignments(result value) returns stream<Course_Assignment, error?>|error {
        io:println(value.message);
        var  x = int:fromString(value.code);
        int code;
        if(x is error){code = 0;} else {code = x;}
        Course_Assignment [] lectAss = [];
        foreach Course_Assignment item in SAssignmentsDataBase {
            if (item.Assignment_Id == code) {
                lectAss.push(item);
            }
        }
        return lectAss.toStream();
    }

    remote function create_courses(stream<Course, grpc:Error?> clientStream) returns stream<result, error?>|error {
        result[] results = [];
        check clientStream.forEach(function(Course c) {
            CourseDataBase.push(c);
            results.push({code: "200", message: "All Courses Created Successfully"});
        });
        return results.toStream();
    }
}
 
 
const string DSA_COURSEMANAGMENTSYSTEM_DESC = "0A1F4453415F436F757273654D616E61676D656E7453797374656D2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F2295010A0455736572121D0A0A557365725F496E646578180120012805520955736572496E64657812170A07557365725F49641802200128095206557365724964121B0A09557365725F5479706518032001280952085573657254797065121B0A09557365725F436F6465180420012809520855736572436F6465121B0A09557365725F4E616D651805200128095208557365724E616D6522E5010A11436F757273655F41737369676E6D656E7412230A0D41737369676E6D656E745F4964180120012805520C41737369676E6D656E74496412270A0F41737369676E6D656E745F4E616D65180220012809520E41737369676E6D656E744E616D6512240A0D6861734265656E4D61726B6564180320012808520D6861734265656E4D61726B656412120A044D61726B18042001280152044D61726B122B0A1141737369676E6D656E745F776569676874180520012805521041737369676E6D656E74576569676874121B0A09557365725F436F6465180620012809520855736572436F646522AD010A06436F75727365121B0A09436F757273655F49641801200128055208436F757273654964121F0A0B436F757273655F4E616D65180220012809520A436F757273654E616D6512400A124C6973745F4F665F41737365736D656E747318032003280B32122E436F757273655F41737369676E6D656E7452104C6973744F6641737365736D656E747312230A0D4173736573736F725F436F6465180420012805520C4173736573736F72436F646522360A06726573756C7412120A04636F64651801200128095204636F646512180A076D65737361676518022001280952076D65737361676532BA020A03434D5312260A0E6372656174655F636F757273657312072E436F757273651A072E726573756C742801300112240A0E61737369676E5F636F757273657312072E436F757273651A072E726573756C74280112200A0C6372656174655F757365727312052E557365721A072E726573756C74280112330A127375626D69745F61737369676E6D656E747312122E436F757273655F41737369676E6D656E741A072E726573756C74280112340A13726571756573745F61737369676E6D656E747312072E726573756C741A122E436F757273655F41737369676E6D656E743001122D0A0C7375626D69745F6D61726B7312122E436F757273655F41737369676E6D656E741A072E726573756C74280112290A08726567697374657212122E436F757273655F41737369676E6D656E741A072E726573756C742801620670726F746F33";

# Description
public isolated client class CMSClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, DSA_COURSEMANAGMENTSYSTEM_DESC);
    }

    isolated remote function assign_courses() returns Assign_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/assign_courses");
        return new Assign_coursesStreamingClient(sClient);
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function submit_assignments() returns Submit_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/submit_assignments");
        return new Submit_assignmentsStreamingClient(sClient);
    }

    isolated remote function submit_marks() returns Submit_marksStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/submit_marks");
        return new Submit_marksStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CMS/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function request_assignments(result|ContextResult req) returns stream<Course_Assignment, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        result message;
        if req is ContextResult {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("CMS/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        Course_AssignmentStream outputStream = new Course_AssignmentStream(result);
        return new stream<Course_Assignment, grpc:Error?>(outputStream);
    }

    isolated remote function request_assignmentsContext(result|ContextResult req) returns ContextCourse_AssignmentStream|grpc:Error {
        map<string|string[]> headers = {};
        result message;
        if req is ContextResult {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("CMS/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        Course_AssignmentStream outputStream = new Course_AssignmentStream(result);
        return {content: new stream<Course_Assignment, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function create_courses() returns Create_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("CMS/create_courses");
        return new Create_coursesStreamingClient(sClient);
    }
}

public client class Assign_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse(Course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse(ContextCourse message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUser(User message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUser(ContextUser message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse_Assignment(Course_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse_Assignment(ContextCourse_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_marksStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse_Assignment(Course_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse_Assignment(ContextCourse_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse_Assignment(Course_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse_Assignment(ContextCourse_Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class Course_AssignmentStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|Course_Assignment value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|Course_Assignment value;|} nextRecord = {value: <Course_Assignment>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Create_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse(Course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse(ContextCourse message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResult() returns result|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <result>payload;
        }
    }

    isolated remote function receiveContextResult() returns ContextResult|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <result>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class CMSResultCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendResult(result response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextResult(ContextResult response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class CMSCourseAssignmentCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCourse_Assignment(Course_Assignment response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCourse_Assignment(ContextCourse_Assignment response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextResultStream record {|
    stream<result, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserStream record {|
    stream<User, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourseStream record {|
    stream<Course, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourse_AssignmentStream record {|
    stream<Course_Assignment, error?> content;
    map<string|string[]> headers;
|};

public type ContextResult record {|
    result content;
    map<string|string[]> headers;
|};

public type ContextUser record {|
    User content;
    map<string|string[]> headers;
|};

public type ContextCourse record {|
    Course content;
    map<string|string[]> headers;
|};

public type ContextCourse_Assignment record {|
    Course_Assignment content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type result record {|
    string code = "";
    string message = "";
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type User record {|
    int User_Index = 0;
    string User_Id = "";
    string User_Type = "";
    string User_Code = "";
    string User_Name = "";
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type Course_Assignment record {|
    int Assignment_Id = 0;
    string Assignment_Name = "";
    boolean hasBeenMarked = false;
    float Mark = 0.0;
    int Assignment_weight = 0;
    string User_Code = "";
|};

@protobuf:Descriptor {value: DSA_COURSEMANAGMENTSYSTEM_DESC}
public type Course record {|
    int Course_Id = 0;
    string Course_Name = "";
    Course_Assignment[] List_Of_Assesments = [];
    int Assessor_Code = 0;
|};

